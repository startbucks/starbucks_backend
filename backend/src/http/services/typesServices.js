import dbContext from '../../context/dbContext';
const { types } = dbContext.Models;

class TypesServices {
    getTypes = async () => {
        try {
            const typesResponse = await types.findAll({
                attributes: ['typeId', 'typeName', 'category'],
            });
            return ({ status: 200, message: 'susses', data: typesResponse });
        }
        catch (error) {
            return ({ status: 500, message: error.message });
        }
    }
    getCategory = async (categoryId) => {
        try {
            const typesResponse = await types.findAll({
                attributes: ['typeId', 'typeName', 'category'],
                where: {
                    category: categoryId,
                }
            });
            return ({ status: 200, message: 'susses', data: typesResponse });
        }
        catch (error) {
            return ({ status: 500, message: error.message });
        }
    }
    postTypes = async (req) => {
        try {
            const typesResponse = await types.bulkCreate(req.body);
            return ({ status: 200, message: 'susses', data: typesResponse });
        } catch (error) {
            return ({ status: 400, message: error.message });
        }
    }

    patchType = async (req) => {
        try {
            const { typeName, category } = req.body;
            const id = req.params.id;
            await types.update({
                typeName:typeName,
                category:category,
            }, {
                where: {
                    typeId: id,
                }
            });
            return ({ status: 201, message: 'susses' });
        } 
        catch (error) {
            return ({ status: 400, message: error.message });
        }
    }
    deleteType = async (req) => {
        try {
            const { typeId } = req.body;
            await types.destroy({
                where: {
                    typeId: typeId,
                }
            });
            console.log(typeId);
            return ({ status: 201, message: 'susses' });
        } 
        catch (error) {
            return ({ status: 400, message: error.message });
        }
    }
}

export default TypesServices;

