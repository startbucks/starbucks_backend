const beverages = (sequelize, DataTypes) => {
    const Beverages = sequelize.define('beverages', {
        beverageId: { // 飲料編號
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        beverageName: { //飲料名稱
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        beverageEnglishName: { //飲料英文名稱
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        beverageDescription: { //飲料內容
            allowNull: false,
            type: DataTypes.STRING(500),
        },
        beveragePrice: { //飲料價格
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        typeId: { // 類別編號(流水號)
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        size: {
            allowNull: false,
            type: DataTypes.STRING(3),
        },
        status: {
            allowNull: false,
            type: DataTypes.BOOLEAN,
        }
    }, {
        timestamps: true, // 是否自動建立時間戳記
    });

    Beverages.associate = (model) => {
        Beverages.belongsTo(model.types, {foreignKey: 'typeId'})
    }

    return Beverages;
}

export { beverages }