import { Router } from 'express';
import AdminsController from '../http/controllers/AdminsController';

const router = Router();
const adminsController = new AdminsController();

router.post('/login', adminsController.Login);
router.get('/', adminsController.Index);
router.post('/', adminsController.AddAdmin);
router.put('/:id', adminsController.UpdateAdmin);

export default router;