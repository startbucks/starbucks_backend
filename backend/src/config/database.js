import dotenv from 'dotenv';

const { DB_CONNECTION, DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD } = dotenv.config().parsed;

const db = {
    connection: DB_CONNECTION,
    host: DB_HOST,
    database: DB_DATABASE,
    username: DB_USERNAME,
    password: DB_PASSWORD
}

export default db;