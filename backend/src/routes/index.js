import { Router } from 'express';

import adminManage from './adminManage';
import company from './company';
import activities from './activities';
import foods from '../routes/foods';
import types from '../routes/types';
import beverages from '../routes/beverages';

const router = Router();

router.use('/adminManage', adminManage);
router.use('/company', company);
router.use('/activities', activities);
router.use('/types', types);
router.use('/foods', foods);
router.use('/beverages', beverages);

export default router;