import { Router } from 'express';
// import userController進入Router
import BeverageController from '../http/Controllers/beveragesController';

const router = Router();
// 建立UserController此Class
const beveragesController = new BeverageController();

router.get('/', beveragesController.getBeverages);
router.get('/onebeverage/:id', beveragesController.postOneBeverage);
router.post('/', beveragesController.postBeverage);
router.put('/:id', beveragesController.patchBeverage);
router.delete('/del', beveragesController.deleteBeverage);
export default router;
