import { Beverages } from "./../models/beveragesModel";
import { Foods } from "./../models/foodsModel";

const types = (sequelize, DataTypes) => {
    const Types = sequelize.define('types', {
        typeId: { // 類別編號
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        typeName: { //類別名稱
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        category: { //商品類別 1or2
            allowNull: false,
            type: DataTypes.INTEGER,
        }
    }, {
        timestamps: false, // 是否自動建立時間戳記
    });
    types.associate = (model) => {
        types.hasOne(model.Foods, {foreignKey: 'typeId'})
    }
    types.associate = (model) => {
        types.hasOne(model.Beverages, {foreignKey: 'typeId'})
    }

    return Types;
}

export { types }