const companies = (sequelize, DataTypes) => {
    const Companies = sequelize.define('companies', {
        companyId: {
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        companySlogan: {
            allowNull: false,
            type: DataTypes.STRING(100),
        },
        description: {
            allowNull: false,
            type: DataTypes.STRING(500),
        }
    }, {
        timestamps: false, // 是否自動建立時間戳記
    });

    return Companies;
}

export { companies }