import dbContext from '../../context/dbContext';
const { beverages } = dbContext.Models;
const { types } = dbContext.Models;

class beveragesServices {
  getBeverages = async () => {
    try {
      const beveragesResponse = await beverages.findAll({
        attributes: [
          'beverageId',
          'beverageName',
          'beverageEnglishName',
          'beverageDescription',
          'beveragePrice',
          'typeId',
          'size',
          'status',
        ],
        include: { model: types, as: 'type' },
      });
      return { status: 200, message: 'susses', data: beveragesResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  getBeverageSize = async (req) => {
    const { beverageName, size } = req.body;
    try {
      const beveragesResponse = await beverages.findOne({
        attributes: [
          'beverageId',
          'beverageName',
          'beverageEnglishName',
          'beverageDescription',
          'beveragePrice',
          'typeId',
          'size',
          'status',
        ],
        include: { model: types, as: 'type' },
        where: {
          beverageName: beverageName,
          size: size,
        },
      });
      return { status: 200, message: 'susses', data: beveragesResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  getBeverageStatus = async (req) => {
    const { beverageName, size, status } = req.body;
    console.log(req.body);
    try {
      const beveragesResponse = await beverages.findOne({
        attributes: [
          'beverageId',
          'beverageName',
          'beverageEnglishName',
          'beverageDescription',
          'beveragePrice',
          'typeId',
          'size',
          'status',
        ],
        where: {
          beverageName: beverageName,
          size: size,
          status: status,
        },
      });
      return { status: 200, message: 'susses', data: beveragesResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  postOneBeverage = async (beverageId) => {
    // const { beverageId } = req.body;
    try {
      const beveragesResponse = await beverages.findOne({
        attributes: [
          'beverageId',
          'beverageName',
          'beverageEnglishName',
          'beverageDescription',
          'beveragePrice',
          'typeId',
          'size',
          'status',
        ],
        where: {
          beverageId,
        },
      });
      return { status: 201, message: 'susses', data: beveragesResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  postBeverage = async (req) => {
    try {
      const {
        beverageName,
        beverageEnglishName,
        beverageDescription,
        beveragePrice,
        typeId,
        size,
      } = req.body;

      await beverages.create({
        beverageName: beverageName,
        beverageEnglishName: beverageEnglishName,
        beverageDescription: beverageDescription,
        beveragePrice: beveragePrice,
        typeId: typeId,
        size: size,
        status: 1,
      });

      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
  patchBeverage = async (req) => {
    try {
      const {
        beverageName,
        beverageEnglishName,
        beverageDescription,
        beveragePrice,
        typeId,
        size,
        status,
      } = req.body;
      const id = req.params.id;

      await beverages.update(
        {
          beverageName: beverageName,
          beverageEnglishName: beverageEnglishName,
          beverageDescription: beverageDescription,
          beveragePrice: beveragePrice,
          typeId: typeId,
          size: size,
          status: status,
        },
        {
          where: {
            beverageId: id,
          },
        },
      );
      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
  deleteBeverage = async (req) => {
    try {
      const { beverageId } = req.body;
      await beverages.destroy({
        where: {
          beverageId: beverageId,
        },
      });
      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
}
export default beveragesServices;
