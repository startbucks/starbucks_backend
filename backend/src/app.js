import cookieParser from 'cookie-parser';
import express from 'express';
import httpErrors from 'http-errors';
import lessMiddleware from 'less-middleware';
import logger from 'morgan';
import path from 'path';
import indexRouter from './routes/index';
import db from './context/dbContext';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, './../public')));
app.use(express.static(path.join(__dirname, './../public')));

(async () => {
  await db.sequelize.sync();
})();

const cors = require('cors');
app.use(cors({
  origin: '*',
  methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
}));

// class
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(httpErrors(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

export default app;