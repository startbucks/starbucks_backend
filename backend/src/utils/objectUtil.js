// 透過此function可以建立global全域物件
const generateSingletonInstance = (symbol, constFunc) => {

    // 先判斷全域物件裡是否有「symbol」這個key值
    // 如果沒有這個key值，就建立key值並且把constFunc()指定為該Key值的Value。
    if (Object.keys(global).findIndex(element => element === symbol) === -1) {
        global[symbol] = constFunc();
    }

    return global[symbol];
}

export { generateSingletonInstance };