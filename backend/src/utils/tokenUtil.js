import jwt from 'jsonwebtoken'
import jwtDecode from 'jwt-decode';

const GetToken = async (id, account) => {
    const payload = {
        adminId: id,
        account: account,
    };
    const token = await jwt.sign({ payload, exp: Math.floor(Date.now() / 1000) + (60 * 15) }, 'my_secret_key');
    return token;
}

const VerifyToken = async (token) => {
    console.log(token)
    if (token)
        return true;
    else
        return false;
    // var decoded = await jwtDecode(token);
    // console.log(decoded)
    // const checkToken = await GetToken(id, account);
    // if (checkToken === rightToken)
    //     return true;
    // else
    //     return false;
}

export { GetToken, VerifyToken };