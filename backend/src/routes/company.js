import { Router } from 'express';
import CompaniesController from '../http/controllers/CompaniesController';

const router = Router();
const companiesController = new CompaniesController();

router.get('/', companiesController.Index);
router.put('/', companiesController.UpdateCompany);

export default router;