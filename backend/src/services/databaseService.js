// 引入 sequelize
import Sequelize from 'sequelize'; 

// 引入 databaseConfig
import dbConfig from './../config/database';
// 引入 generateSingletonInstance，此方法可以將Function加入全域變數
import { generateSingletonInstance } from './../utils/objectUtil';

class databaseService {
    connect = () => {
        // 透過Sequelize建立資料庫連線
        const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
            host: dbConfig.host, // 連線路徑
            dialect: dbConfig.connection // 資料庫類型
        });

        return sequelize;
    }
}

// 最後將此方法將入全域變數global裡。
export default generateSingletonInstance('SERVICES_DATABASESERVICE', () => new databaseService());