const getTodayDate = async () => {
    let Today = new Date();
    let todayDate = await Today.getFullYear() + "-" + (Today.getMonth()+1) + "-" + Today.getDate();
    return todayDate;
}

export { getTodayDate };