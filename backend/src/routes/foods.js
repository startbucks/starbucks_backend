import { Router } from 'express';
// import userController進入Router
import FoodsController from './../http/Controllers/foodsController';

const router = Router();
// 建立UserController此Class
const foodsController = new FoodsController();

router.get('/', foodsController.getFoods);
router.get('/onefood/:id', foodsController.postOneFood);
router.post('/', foodsController.postFood);
router.put('/:id', foodsController.patchFood);
router.delete('/del', foodsController.deleteFoods);
export default router;
