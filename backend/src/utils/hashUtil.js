import sha256 from 'sha256';

const getHash = async (origin) => {
    const hash = await sha256(origin);
    return hash;
}

const compare = async (unhashString, compareString) => {
    const hashString = await getHash(unhashString);
    if (hashString === compareString) 
        return true;
    else{
        return false;
    }
}

export { getHash, compare };