import { GetToken } from "../../utils/tokenUtil";
import { getHash, compare } from "../../utils/hashUtil";
import { getTodayDate } from "../../utils/dateUtil";
import dbContext from "../../context/dbContext";
const { admins } = dbContext.Models;

class AdminsService {
    Login = async (req) => {
        try {
            const { account, password } = req;
            const adminResponse = await admins.findOne({
                attributes: ['adminId', 'account', 'password', 'name', 'createAt', 'status']
            },{
                where: {
                    account: account
                }
            })
            const isCorrect = await compare(password, adminResponse.password);
            if (isCorrect) {
                const token = await GetToken(adminResponse.adminId, adminResponse.account);
                return ({ status: 200, message: 'login success', token: token });
            }
            else
                return ({ status: 400, message: 'login fail' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }

    GetAllAdmins = async () => {
        try {
            const adminsResponse = await admins.findAll({
                attributes: ['adminId', 'account', 'name', 'createAt', 'status']
            });
            return ({ status: 200, message: 'success', data: adminsResponse });
        }
        catch (error) {
            return ({ status: 500, message: error });
        }
    }

    CreateAdmin = async (req) => {
        try {
            const { name, account, password } = req;
            await admins.create({
                name: name,
                account: account,
                password: await getHash(password),
                status: true,
                createAt: await getTodayDate()
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }

    UpdateAdmin = async (req) => {
        try {
            const id = req.params.id;
            const { name, status } = req.body;
            await admins.update({
                name: name,
                status: status
            },
            {
                where: {
                    adminId: id
                }
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }
    
}

export default AdminsService;