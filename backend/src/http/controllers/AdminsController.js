import { VerifyToken } from "../../utils/tokenUtil";
import { isCheck } from "../../utils/checkKeyUtil";
import AdminsService from "../services/AdminsService";
const adminsService = new AdminsService();

class AdminsController {
    Login = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const bodyKey = ["account", "password"];
        if (isCheck(bodyKey, req)) {
            const response = await adminsService.Login(req.body);
            if (response.status === 200) {
                res.status(200).json(response);
                return;
            }
            else {
                res.status(400).json(response);
                return;
            }
        }
        else {
            res.status(400).json({
                status: 400,
                message: '欄位遺失，請確認是否有缺少欄位'
            });
        }
    }

    Index = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const response = await adminsService.GetAllAdmins();

            if (response.status === 200) 
                res.status(200).json(response);
            else
                res.status(500).json(response);
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }

    AddAdmin = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const bodyKey = ["name", "account", "password"];

            if (isCheck(bodyKey, req)) {
                const response = await adminsService.CreateAdmin(req.body);
                if (response.status === 201) 
                    res.status(201).json(response);
                else
                    res.status(400).json(response);
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: '欄位遺失，請確認是否有缺少欄位'
                });
            }
    }

    UpdateAdmin = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const bodyKey = ["name", "status"];

            if (isCheck(bodyKey, req)) {
                const response = await adminsService.UpdateAdmin(req);
                if (response.status === 201) 
                    res.status(201).json(response);
                else
                    res.status(400).json(response);
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: '欄位遺失，請確認是否有缺少欄位'
                });
            }
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }

}

export default AdminsController;