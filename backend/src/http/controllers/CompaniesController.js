import { VerifyToken } from "../../utils/tokenUtil";
import { isCheck } from "../../utils/checkKeyUtil";
import CompaniesService from "../services/companiesService";
const companiesService = new CompaniesService();

class CompaniesController {
    Index = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const response = await companiesService.GetCompany();

            if (response.status === 200) 
                res.status(200).json(response);
            else
                res.status(500).json(response);
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }

    UpdateCompany = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const bodyKey = ["companySlogan", "description"];

            if (isCheck(bodyKey, req)) {
                const isExist = await companiesService.GetCompany();
                if (isExist.data != null) {
                    const response = await companiesService.UpdateCompany(req);
                    if (response.status === 201) 
                        res.status(201).json(response);
                    else
                        res.status(400).json(response);
                }
                else {
                    const response = await companiesService.CreateCompany(req);
                    if (response.status === 201) 
                        res.status(201).json(response);
                    else
                        res.status(400).json(response);
                }
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: '欄位遺失，請確認是否有缺少欄位'
                });
            }
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }
}

export default CompaniesController;