import e from 'express';
import { VerifyToken } from '../../utils/tokenUtil';
import BeveragesServies from '../services/beveragesServices';

const beveragesServies = new BeveragesServies();

class beveragesController {
  getBeverages = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    const response = await beveragesServies.getBeverages(req);
    console.log(response);
    if (response.status === 200) res.status(200).json(response);
    else res.status(500).json(response);
  };
  postOneBeverage = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    // // const bodyKey = ["beverageId"];
    // let checkedKey = true;
    // bodyKey.forEach(key => {
    //     if (Object.keys(req.body).findIndex(element => element === key) === -1) {
    //         checkedKey = false;
    //         return;
    //     }
    // });
    // console.log(req.body);
    // if (!checkedKey) {
    //     res.status(400).json({
    //         status: 400,
    //         message: '欄位遺失，請確認是否有缺少欄位'
    //     });
    //     return;
    // }
    const { id } = req.params;
    const response = await beveragesServies.postOneBeverage(id);
    if (response.status === 201) res.status(201).json(response);
    else res.status(400).json(response);
  };
  postBeverage = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      const bodyKey = [
        'beverageName',
        'beverageEnglishName',
        'beverageDescription',
        'beveragePrice',
        'typeId',
      ];
      let checkedKey = true;
      const size = await beveragesServies.getBeverageSize(req);
      if (size.data !== null) {
        res.status(400).json({
          status: 400,
          message: '此容量已存在',
        });
      } else {
        const response = await beveragesServies.postBeverage(req);
        if (response.status === 201) res.status(201).json(response);
        else res.status(400).json(response);
      }
      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });
      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
  patchBeverage = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const bodyKey = [
      'beverageName',
      'beverageEnglishName',
      'beverageDescription',
      'beveragePrice',
      'typeId',
      'status',
    ];
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      let checkedKey = true;
      const size = await beveragesServies.getBeverageStatus(req);
      const response = await beveragesServies.patchBeverage(req);
      if (response.status === 201) res.status(201).json(response);
      else res.status(400).json(response);
      // if (size.data !== null) {
      //   console.log('qqqqqqqqqq')
      //   res.status(400).json({
      //     status: 400,
      //     message: '此容量已存在',
      //   });
      // } else {
      //   const response = await beveragesServies.patchBeverage(req);
      //   if (response.status === 201) res.status(201).json(response);
      //   else res.status(400).json(response);
      // }
      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });
      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
  deleteBeverage = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const bodyKey = ['beverageId'];
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      let checkedKey = true;

      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });
      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
      const response = await beveragesServies.deleteBeverage(req);
      console.log(req.body);
      if (response.status === 201) res.status(201).json(response);
      else res.status(400).json(response);
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
}

export default beveragesController;
