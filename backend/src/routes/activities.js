import { Router } from 'express';
import ActivitiesController from '../http/controllers/ActivitiesController';

const router = Router();
const activitiesController = new ActivitiesController();

router.get('/', activitiesController.Index);
router.get('/:id', activitiesController.Activity);
router.post('/', activitiesController.AddActivity);
router.put('/:id', activitiesController.UpdateActivity);

export default router;