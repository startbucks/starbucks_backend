const foods = (sequelize, DataTypes) => {
    const Foods = sequelize.define('foods', {
        foodId: { // 商品編號
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        foodName: { //商品名稱
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        foodEnglishName: { //商品英文名稱
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        foodDescription: { //商品內容
            allowNull: false,
            type: DataTypes.STRING(500),
        },
        foodPrice: { //商品價格
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        typeId: { // 類別編號(流水號)
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        status: {
            allowNull: false,
            type: DataTypes.BOOLEAN,
        },
    }, {
        timestamps: true, // 是否自動建立時間戳記
    });
    
    Foods.associate = (model) => {
        Foods.belongsTo(model.types, {foreignKey: 'typeId'})
    }

    return Foods;
}


export { foods }