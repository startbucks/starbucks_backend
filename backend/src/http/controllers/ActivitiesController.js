import { VerifyToken } from "../../utils/tokenUtil";
import { isCheck } from "../../utils/checkKeyUtil";
import ActivitiesService from "../services/ActivitiesService";
const activitiesService = new ActivitiesService();

class ActivitiesController {
    
    Index = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const response = await activitiesService.GetAllActivities();

        if (response.status === 200) 
            res.status(200).json(response);
        else
            res.status(500).json(response);
    }

    Activity = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const response = await activitiesService.GetActivity(req);

        if (response.status === 200) 
            res.status(200).json(response);
        else
            res.status(500).json(response);
    }

    AddActivity = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const bodyKey = ["activityName", "activityContent", "activityStartDate", "activityEndDate", "adminId"];

            if (isCheck(bodyKey, req)) {
                const response = await activitiesService.CreateActivity(req.body);
                if (response.status === 201) 
                    res.status(201).json(response);
                else
                    res.status(400).json(response);
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: '欄位遺失，請確認是否有缺少欄位'
                });
            }
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }

    UpdateActivity = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const isVerified = await VerifyToken(req.header('Authorization'));
        if (isVerified) {
            const bodyKey = ["activityName","activityContent", "activityStartDate", "activityEndDate", "adminId"];

            if (isCheck(bodyKey, req)) {
                const response = await activitiesService.UpdateActivity(req);
                if (response.status === 201) 
                    res.status(201).json(response);
                else
                    res.status(400).json(response);
            }
            else {
                res.status(400).json({
                    status: 400,
                    message: '欄位遺失，請確認是否有缺少欄位'
                });
            }
        }
        else {
            res.status(401).json({
                status: 401,
                message: 'please login first'
            });
        }
    }

}

export default ActivitiesController;