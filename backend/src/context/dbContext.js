import { Sequelize, DataTypes } from 'sequelize';
import databaseService from '../services/databaseService';

import { admins } from '../models/adminsModel';
import { activities } from '../models/activitiesModel';
import { companies } from '../models/companiesModel';
import { foods } from '../models/foodsModel';
import { types } from '../models/typesModel';
import { beverages } from '../models/beveragesModel';

import _ from 'lodash';

class Database {
    // 透過constructor(建構子)的方式，當該Class變實作時，會自動執行
    constructor() {
        const db = {};
        // 開始連線
        const sequelize = databaseService.connect();

        db.Models = {};
        db.Models.admins = admins(sequelize, DataTypes);
        db.Models.activities = activities(sequelize, DataTypes);
        db.Models.companies = companies(sequelize, DataTypes);
        db.Models.foods = foods(sequelize, DataTypes);
        db.Models.types = types(sequelize, DataTypes);
        db.Models.beverages = beverages(sequelize, DataTypes);

        
        // Object.keys(db.Models).forEach((i) => {
        //     // console.log(db.Models[i])
        //     if (db.Models[i].associate) {
        //         db.Models[i].associate(db.Models);
        //     }
        // })
        _.map(db.Models,(item)=>{
            if(item.associate){
                item.associate(db.Models)
            }
        })

        // 建立ORM映射環境
        db.Sequelize = Sequelize;
        db.sequelize = sequelize;

        return db;
    }
}

export default new Database();