import { getTodayDate } from "../../utils/dateUtil";
import dbContext from "../../context/dbContext";
const { activities, admins } = dbContext.Models;

class ActivitiesService {

    GetAllActivities = async () => {
        try {
            const activitiesResponse = await activities.findAll({
                attributes: ['activityId', 'activityName', 'activityContent', 'activityStartDate', 'activityEndDate', 'createAt'],
                include: {
                    model: admins,
                    attributes: ['adminId', 'name']
                }
            });
            
            return ({ status: 200, message: 'success', data: activitiesResponse });
        }
        catch (error) {
            console.log(error)
            return ({ status: 500, message: error });
        }
    }

    GetActivity = async (req) => {
        try {
            const id = req.params.id;
            const activitiesResponse = await activities.findOne({
                attributes: ['activityId', 'activityName', 'activityContent', 'activityStartDate', 'activityEndDate', 'createAt'],
                where: {
                    activityId: id
                },
                include: {
                    model: admins,
                    attributes: ['adminId', 'name']
                }
            });
            
            return ({ status: 200, message: 'success', data: activitiesResponse });
        }
        catch (error) {
            console.log(error)
            return ({ status: 500, message: error });
        }
    }

    CreateActivity = async (req) => {
        try {
            const { activityName, activityContent, activityStartDate, activityEndDate, adminId } = req;
            await activities.create({
                activityName: activityName,
                activityContent: activityContent,
                activityStartDate: activityStartDate,
                activityEndDate: activityEndDate,
                adminId: adminId,
                createAt: await getTodayDate()
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }

    UpdateActivity = async (req) => {
        try {
            const id = req.params.id;
            const { activityName, activityContent, activityStartDate, activityEndDate, adminId } = req.body;
            await activities.update({
                activityName: activityName,
                activityContent: activityContent,
                activityStartDate: activityStartDate,
                activityEndDate: activityEndDate,
                adminId: adminId,
            },
            {
                where: {
                    activityId: id
                }
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }
    
}

export default ActivitiesService;