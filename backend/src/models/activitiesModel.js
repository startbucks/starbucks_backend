const activities = (sequelize, DataTypes) => {
    const Activities = sequelize.define('activities', {
        activityId: {
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        activityName: {
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        activityContent: {
            allowNull: false,
            type: DataTypes.STRING(500),
        },
        activityStartDate: {
            allowNull: false,
            type: DataTypes.DATE,
        },
        activityEndDate: {
            allowNull: true,
            type: DataTypes.DATE,
        },
        adminId: {
            allowNull: false,
            type: DataTypes.INTEGER,
        },
        createAt: {
            allowNull: false,
            type: DataTypes.DATE,
        }

    }, {
        timestamps: false, // 是否自動建立時間戳記
    });

    Activities.associate = (model) => {
        Activities.belongsTo(model.admins, {foreignKey: 'adminId'})
    }

    return Activities;
}

export { activities }