import { Router } from 'express';
import TypesController from './../http/controllers/typesController';
const router = Router();
const typesController = new TypesController();


router.get('/', typesController.getTypes);
router.get('/category/:id', typesController.getCategory);
router.post('/', typesController.postTypes);
router.put('/:id', typesController.patchType);
router.delete('/del', typesController.deleteType);

export default router;