import { getHash, compare } from "../../utils/hashUtil";
import dbContext from "../../context/dbContext";
const { companies } = dbContext.Models;

class CompaniesService {
    GetCompany = async () => {
        try {
            const companyResponse = await companies.findOne({
                attributes: ['companySlogan', 'description']
            });
            return ({ status: 200, message: 'success', data: companyResponse });
        }
        catch (error) {
            return ({ status: 500, message: error });
        }
    }

    CreateCompany = async (req) => {
        try {
            const { companySlogan, description } = req.body;
            await companies.create({
                companySlogan: companySlogan,
                description: description
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }

    UpdateCompany = async (req) => {
        try {
            const { companySlogan, description } = req.body;
            await companies.update({
                companySlogan: companySlogan,
                description: description
            },{
                where: {
                    companyId: 1
                }
            });
            return ({ status: 201, message: 'success' });
        }
        catch (error) {
            return ({ status: 400, message: error });
        }
    }
}

export default CompaniesService;