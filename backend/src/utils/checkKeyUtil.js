const isCheck = async (bodyKey, req) => {
    await bodyKey.forEach(key => {
        if (Object.keys(req.body).findIndex(element => element == key) === -1) {
            return false;
        }
        return true;
    });
}

export { isCheck };