import TypesServies from '../services/typesServices';
import { isCheck } from "../../utils/checkKeyUtil";

const typesServies = new TypesServies();


class TypesController {
    getTypes = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
    
        const response = await typesServies.getTypes(req);
        console.log(response);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(500).json(response);
    }
    getCategory = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        const { id } = req.params;
        const response = await typesServies.getCategory(id);
        console.log(response);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(500).json(response);
    }
    postTypes = async (req, res) => {
        res.setHeader('Content-Type', 'application/json'); 
        const response = await typesServies.postTypes(req);
        if (response.status === 201)
            res.status(201).json(response);
        else
            res.status(400).json(response);
    }

    patchType = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        //#region 確認欄位是否存在
        const bodyKey = ["typeName", "category"];
        let checkedKey = true;

        bodyKey.forEach(key => {
            if (Object.keys(req.body).findIndex(element => element === key) === -1) {
                checkedKey = false;
                return;
            }
        });

        if (!checkedKey) {
            res.status(400).json({
                status: 400,
                message: '欄位遺失，請確認是否有缺少欄位'
            });
            return;
        }
        //#endregion

        const response = await typesServies.patchType(req);

        if (response.status === 201)
            res.status(201).json(response);
        else
            res.status(400).json(response);
    }
    
    deleteType = async (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        //#region 確認欄位是否存在
        const bodyKey = ["typeId"];
        let checkedKey = true;

        bodyKey.forEach(key => {
            if (Object.keys(req.body).findIndex(element => element === key) === -1) {
                checkedKey = false;
                return;
            }
        });
        if (!checkedKey) {
            res.status(400).json({
                status: 400,
                message: '欄位遺失，請確認是否有缺少欄位'
            });
            return;
        }
        const response = await typesServies.deleteType(req);
        console.log(req.body);
        if (response.status === 201)
            res.status(201).json(response);
        else
            res.status(400).json(response);
    }
}
 

export default TypesController;