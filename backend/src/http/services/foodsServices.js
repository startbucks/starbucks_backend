import dbContext from '../../context/dbContext';
const { foods } = dbContext.Models;
const { types } = dbContext.Models;

class foodsServices {
  getFoods = async () => {
    try {
      const foodsResponse = await foods.findAll({
        attributes: [
          'foodId',
          'foodName',
          'foodEnglishName',
          'foodDescription',
          'foodPrice',
          'typeId',
          'status',
        ],
        include: { model: types, as: 'type' },
      });
      return { status: 200, message: 'susses', data: foodsResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  getFoodType = async (req) => {
    const { foodName, typeId } = req.body;
    // console.log(req.body);
    try {
      const foodsResponse = await beverages.findOne({
        attributes: [
          'foodId',
          'foodName',
          'foodEnglishName',
          'foodDescription',
          'foodPrice',
          'typeId',
          'status',
        ],
        where: {
          foodName: foodName,
          typeId: typeId,
        },
      });
      return { status: 200, message: 'susses', data: foodsResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };

  /* 取得特定食物 
    @param {string} foodId
    @returns {object}
  */
  postOneFood = async (foodId) => {
    // const id = req.params.id;
    console.log(foodId);
    try {
      const foodsResponse = await foods.findOne({
        attributes: [
          'foodId',
          'foodName',
          'foodEnglishName',
          'foodDescription',
          'foodPrice',
          'typeId',
          'status',
        ],
        include: { model: types, as: 'type' },
        where: {
          foodId,
        },
      });
      console.log(foodsResponse);
      return { status: 201, message: 'susses', data: foodsResponse };
    } catch (error) {
      return { status: 500, message: error.message };
    }
  };
  postFood = async (req) => {
    try {
      const { foodName, foodEnglishName, foodDescription, foodPrice, typeId } =
        req.body;
      await foods.create({
        foodName: foodName,
        foodEnglishName: foodEnglishName,
        foodDescription: foodDescription,
        foodPrice: foodPrice,
        typeId: typeId,
        status: 1,
      });

      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
  patchFood = async (req) => {
    try {
      const {
        foodName,
        foodEnglishName,
        foodDescription,
        foodPrice,
        typeId,
        status,
      } = req.body;
      const id = req.params.id;
      await foods.update(
        {
          foodName: foodName,
          foodEnglishName: foodEnglishName,
          foodDescription: foodDescription,
          foodPrice: foodPrice,
          typeId: typeId,
          status: status,
        },
        {
          where: {
            foodId: id,
          },
        },
      );
      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
  deleteFoods = async (req) => {
    try {
      const { foodId } = req.body;
      await foods.destroy({
        where: {
          foodId: foodId,
        },
      });
      return { status: 201, message: 'susses' };
    } catch (error) {
      return { status: 400, message: error.message };
    }
  };
}
export default foodsServices;
