import FoodsServies from '../services/foodsServices';
import { VerifyToken } from '../../utils/tokenUtil';

const foodsServies = new FoodsServies();

class foodsController {
  getFoods = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const response = await foodsServies.getFoods(req);
    console.log(response);
    if (response.status === 200) res.status(200).json(response);
    else res.status(500).json(response);
  };
  postOneFood = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const { id } = req.params;
    const response = await foodsServies.postOneFood(id);
    if (response.status === 201) res.status(201).json(response);
    // else res.status(400).json(response);
  };
  postFood = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const bodyKey = [
      'foodName',
      'foodEnglishName',
      'foodDescription',
      'foodPrice',
      'typeId',
    ];
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      const typeId = await foodsServies.getFoodType(req);
      if (typeId.data != null) {
        res.status(400).json({
          status: 400,
          message: '此商品已存在',
        });
      } else {
        const response = await foodsServies.postFood(req);
        if (response.status === 201) {
          res.status(201).json(response);
          return;
        } else {
          res.status(400).json(response);
          return;
        }
      }
      let checkedKey = true;
      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });
      console.log(req.body);
      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
  patchFood = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const bodyKey = [
      'foodName',
      'foodEnglishName',
      'foodDescription',
      'foodPrice',
      'typeId',
      'status',
    ];
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      let checkedKey = true;
      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });

      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
      //#endregion

      const response = await foodsServies.patchFood(req);

      if (response.status === 201) res.status(201).json(response);
      else res.status(400).json(response);
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
  deleteFoods = async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const bodyKey = ['foodId'];
    const isVerified = await VerifyToken(req.header('Authorization'));
    if (isVerified) {
      let checkedKey = true;

      bodyKey.forEach((key) => {
        if (
          Object.keys(req.body).findIndex((element) => element === key) === -1
        ) {
          checkedKey = false;
          return;
        }
      });
      if (!checkedKey) {
        res.status(400).json({
          status: 400,
          message: '欄位遺失，請確認是否有缺少欄位',
        });
        return;
      }
      const response = await foodsServies.deleteFoods(req);
      console.log(req.body);
      if (response.status === 201) res.status(201).json(response);
      else res.status(400).json(response);
    } else {
      res.status(401).json({
        status: 401,
        message: 'please login first',
      });
    }
  };
}

export default foodsController;
